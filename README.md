# update-sg-ips-cloudflare

Lambda function to automatically update security groups to allow Cloudflare IPs

## Setup:
1. Create Lambda function with IAM policies for:
    ```
    ec2:RevokeSecurityGroupIngress
    ec2:AuthorizeSecurityGroupIngress
    ec2:DescribeSecurityGroups
    ```
2. Set `LB_SECURITY_GROUP_ID` environment variable to your security group ID
3. Optionally, create an EventBridge rule to run the Lambda on a schedule to sync the security group automatically
