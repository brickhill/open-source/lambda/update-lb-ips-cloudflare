const AWS = require('aws-sdk')
const ec2 = new AWS.EC2({apiVersion: '2016-11-15'});

const cloudflare = require('./api/cloudflare');

async function handler() {
    let ips = await cloudflare.getIPs();
    let groupData = await ec2.describeSecurityGroups({
        GroupIds: [process.env.LB_SECURITY_GROUP_ID]
    }).promise();

    await removeNonCFIPs(ips, groupData);

    let port80 = groupData.SecurityGroups[0].IpPermissions.find(perm => perm.FromPort == 80) || {IpRanges: [], Ipv6Ranges: []};
    let port443 = groupData.SecurityGroups[0].IpPermissions.find(perm => perm.FromPort == 443) || {IpRanges: [], Ipv6Ranges: []};
    let filteredIps = {
        v4: {
            80: ips.ipv4_cidrs.filter(ip => !port80.IpRanges.some(range => range.CidrIp == ip)).map(ip => ({"CidrIp": ip})),
            443: ips.ipv4_cidrs.filter(ip => !port443.IpRanges.some(range => range.CidrIp == ip)).map(ip => ({"CidrIp": ip}))
        },
        v6: {
            80: ips.ipv6_cidrs.filter(ip => !port80.Ipv6Ranges.some(range => range.CidrIpv6 == ip)).map(ip => ({"CidrIpv6": ip})),
            443: ips.ipv6_cidrs.filter(ip => !port443.Ipv6Ranges.some(range => range.CidrIpv6 == ip)).map(ip => ({"CidrIpv6": ip}))
        }
    }

    let ingress = {
        GroupId: process.env.LB_SECURITY_GROUP_ID,
        IpPermissions: [{
            IpProtocol: "tcp",
            FromPort: 80,
            ToPort: 80,
            IpRanges: ipsOrUndefined(filteredIps.v4['80']),
            Ipv6Ranges: ipsOrUndefined(filteredIps.v6['80'])
        }, {
            IpProtocol: "tcp",
            FromPort: 443,
            ToPort: 443,
            IpRanges: ipsOrUndefined(filteredIps.v4['443']),
            Ipv6Ranges: ipsOrUndefined(filteredIps.v6['443'])
        }]
    }

    await ec2.authorizeSecurityGroupIngress(ingress).promise();
}

async function removeNonCFIPs(ips, groupData) {
    let revokeIngress = {
        GroupId: process.env.LB_SECURITY_GROUP_ID,
        IpPermissions: []
    }
    let len = 0;
    groupData.SecurityGroups[0].IpPermissions.forEach((port, i) => {
        let ipv4 = port.IpRanges.filter(ip => !ips.ipv4_cidrs.includes(ip.CidrIp));
        let ipv6 = port.Ipv6Ranges.filter(ip => !ips.ipv6_cidrs.includes(ip.CidrIpv6));
        revokeIngress.IpPermissions[i] = {
            IpProtocol: port.IpProtocol,
            FromPort: port.FromPort,
            ToPort: port.ToPort,
            IpRanges: ipsOrUndefined(ipv4),
            Ipv6Ranges: ipsOrUndefined(ipv6)
        };
        len += ipv4.length;
        len += ipv6.length;
    });

    if(len > 0)
        await ec2.revokeSecurityGroupIngress(revokeIngress).promise();
}

function ipsOrUndefined(ips) {
    return ips.length > 0 ? ips : undefined
}

module.exports = {
    handler
};