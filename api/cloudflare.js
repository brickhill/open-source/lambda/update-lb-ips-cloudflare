const phin = require('phin')
    .defaults({ parse: 'json', timeout: 12000 })

const CF_API = `https://api.cloudflare.com/client/v4/ips`;

async function getIPs() {
    try {
        const data = (await phin({url: CF_API})).body
        return data.result
    } catch (err) {}
}

module.exports = {
    getIPs
}